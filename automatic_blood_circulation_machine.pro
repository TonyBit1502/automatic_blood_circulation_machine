#-------------------------------------------------
#
# Project created by QtCreator 2017-05-28T15:00:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets charts

TARGET = automatic_blood_circulation_machine
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
