#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QMenuBar>
#include <QAction>
#include <QTimer>
#include <QtCharts>

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
    QVBoxLayout *vbl1, *vbl2, *vbl3, *vbl4, *vbl5, *vbl6, *vbl7;
    QHBoxLayout *hbl1, *hbl2, *hbl3;
    QAction *m_start, *m_pause, *m_stop, *m_exit;
    QLineEdit *ln_edit1, *ln_edit2, *ln_edit3, *ln_edit4, *ln_edit5, *ln_edit6;
    QMenuBar *m_menuBar;
    QMenu *m_menu_1, *m_menu_2, *m_menu_3, *m_menu_4;
    QLabel *l1, *l2, *l3, *l4, *l5, *l6;
    QChartView *chartView;

public slots:
    void startProcess();
    void stopProcess();
    void restartProcess();
    void closeProgram();
signals:

};

#endif // WIDGET_H
