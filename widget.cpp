#include "widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{   // Объявление
    vbl1 = new QVBoxLayout();
    vbl2 = new QVBoxLayout();
    vbl3 = new QVBoxLayout();
    vbl4 = new QVBoxLayout();
    vbl5 = new QVBoxLayout();
    vbl6 = new QVBoxLayout();
    vbl7 = new QVBoxLayout();
    hbl1 = new QHBoxLayout();
    hbl2 = new QHBoxLayout();
    hbl3 = new QHBoxLayout();
    m_start =  new QAction("Start");
    m_pause = new QAction("Pause");
    m_stop = new QAction("Stop");
    m_exit = new QAction("Exit");
    ln_edit1 = new QLineEdit();
    ln_edit2 = new QLineEdit();
    ln_edit3 = new QLineEdit();
    ln_edit4 = new QLineEdit();
    ln_edit5 = new QLineEdit();
    ln_edit6 = new QLineEdit();
    l1 = new QLabel("Вес пациента, кг");
    l2 = new QLabel("q");
    l3 = new QLabel("Pa");
    l4 = new QLabel("Kоб");
    l5 = new QLabel("Действие сосудосужающего препарата");
    l6 = new QLabel("Действие сосудорасширяющего препарата");
    chartView = new QChartView();
    m_menuBar = new QMenuBar();
    m_menu_1 = new QMenu("Start");
    m_menu_2 = new QMenu("Pause");
    m_menu_3 = new QMenu("Stop");
    m_menu_4 = new QMenu("Exit");

    // Компановка
    m_menu_1->addAction(m_start);
    m_menu_2->addAction(m_pause);
    m_menu_3->addAction(m_stop);
    m_menu_4->addAction(m_exit);
    m_menuBar->addMenu(m_menu_1);
    m_menuBar->addMenu(m_menu_2);
    m_menuBar->addMenu(m_menu_3);
    m_menuBar->addMenu(m_menu_4);

    vbl2->addWidget(l1);
    vbl2->addWidget(ln_edit1);
    vbl3->addWidget(l2);
    vbl3->addWidget(ln_edit2);
    vbl4->addWidget(l3);
    vbl4->addWidget(ln_edit3);
    vbl5->addWidget(l4);
    vbl5->addWidget(ln_edit4);
    vbl6->addWidget(l5);
    vbl6->addWidget(ln_edit5);
    vbl7->addWidget(l6);
    vbl7->addWidget(ln_edit6);
    hbl1->addLayout(vbl2);
    hbl1->addLayout(vbl3);
    hbl1->addLayout(vbl4);
    hbl1->addLayout(vbl5);
    hbl1->addLayout(vbl6);
    hbl1->addLayout(vbl7);

    vbl1->addWidget(m_menuBar);
    vbl1->addLayout(hbl1);
    vbl1->addWidget(chartView);
    this->setLayout(vbl1);

    //connect's
    connect(m_start, SIGNAL(triggered()), this, SLOT(startProcess()));
    connect(m_pause, SIGNAL(triggered()), this, SLOT(stopProcess()));
    connect(m_stop, SIGNAL(triggered()), this, SLOT(restartProcess()));
    connect(m_exit, SIGNAL(triggered()), this, SLOT(closeProgram()));

}

Widget::~Widget()
{

}

void Widget::startProcess()
{

}

void Widget::stopProcess()
{

}

void Widget::restartProcess()
{

}

void Widget::closeProgram()
{

}
